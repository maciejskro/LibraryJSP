<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="resources/floating-labels.css"/>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="addBookModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add new book to inventory</h4>
                </div>

                <div class="modal-body form-control">
                    <form action="addbookservlet" method="post" class="form-signin-modal" id="addbook_form"
                          name="dialogAddBook">
                        <div class="modal-body">

                            <div class="form-label-group">
                                <input type="text" class="form-control" aria-labelledby="titleInput" name="title"
                                       id="titleInput" placeholder="Enter title book" required autofocus>
                                <label for="titleInput">Enter book title</label>
                            </div>

                            <div class="form-label-group">
                                <input type="text" class="form-control" aria-labelledby="isbnInput" name="isbn"
                                       id="isbnInput" placeholder="Enter International Standard Book Number" required
                                       autofocus>
                                <label for="isbnInput">Add International Standard Book Number</label>
                            </div>

                            <div class="form-label-group">
                                <select class="custom-select my-2 mr-sm-5" id="categoryInput" name="category"
                                        placeholder="Choose category" aria-labelledby="categoryInput">
                                    <c:forEach items="${sessionScope.category}" var="cat" varStatus="loop">
                                        <option value="${cat}"
                                                aria-labelledby="categoryInput">${cat.description}</option>
                                    </c:forEach>
                                </select>
                                <label for="categoryInput"></label>
                            </div>
                            <div class="form-label-group">
                                <select multiple class="form-control" id="authorListSelect" name="authors">
                                    <c:forEach items="${sessionScope.authorsList}" var="author" varStatus="aloop">
                                        <option value="${author.id}">${author.authorName}</option>
                                        <c:out value="${author}"/>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-label-group">
                                <input type="number" class="form-control" aria-labelledby="pagesInput" name="pages"
                                       id="pagesInput" placeholder="Enter number of pages" required autofocus>
                                <label for="pagesInput">Pages number</label>
                            </div>

                            <div class="form-label-group">
                                <input type="date" class="form-control date-picker" aria-labelledby="releaseInput"
                                       name="release" id="releaseInput" placeholder="Enter release date">
                                <label for="releaseInput">Release date</label>
                            </div>
                            <div class="form-label-group">
                                <textarea class="form-control" aria-labelledby="descriptionInput" name="description" rows="3"
                                          placeholder="Write some description" id="descriptInput"></textarea>
                                <label for="descriptInput"></label>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" id="submitBookForm" value="Add">
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let title = $('#titleInput').var();
    let isbn = $('#isbnInput').var();
    let category = $('#categoryInput').var();
    let authors = $('#authorListSelect').val();

    $(function () {
            $("#submitBookForm").on("submit", function (e) {
                e.preventDefault();
                var postData = $('#addbook_form').serialize();
                var formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data) {
                        $('#addBookModal .modal-header .modal-title').html("Result");
                        $('#addBookModal .modal-body').html(data).update("Saved successfully !");
                        $('#submitBookForm').remove();
                    },
                    error: function (jqXHR, status, error) {
                        console.log(status + ": " + error + " " + authors);
                    }
                });
            });
        }
    );
</script>

