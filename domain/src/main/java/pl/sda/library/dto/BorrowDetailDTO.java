package pl.sda.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.library.entity.Borrower;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BorrowDetailDTO {

    private String address;
    private String city;
    private String email;
    private String phoneNo;
    private Borrower borrower;
}
