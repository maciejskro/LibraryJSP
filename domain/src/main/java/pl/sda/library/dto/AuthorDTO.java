package pl.sda.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthorDTO {

    private Long id;
    @NotNull
    @Length(min = 2)
    private String firstname;
    @NotNull
    private String lastname;

    private String placeOfBorn;

    private LocalDate dateOfBorn;

    public String getAuthorName() {
        return firstname + " " + lastname;
    }


}
