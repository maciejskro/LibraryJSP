package pl.sda.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//import Book;
//import Borrower;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BorrowDTO {

    private LocalDate dateOfBorrow;
//    private Book book;
//    private Borrower borrower;
}
