<%--
  Created by IntelliJ IDEA.
  User: smaciej
  Date: 30.05.18
  Time: 16:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <script src="webjars/jquery-navigation/2.1/nav.js"></script>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand col-1" href="#">Menu</a>
        <ul class="navbar-nav col-2">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn-block" href="#" id="navbarDropdown" role="button" id="dropdownLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Book
                </a>
                <div id="dropdownBookMenu" class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#addBookModal">Add book</button>
                    <button type="button" class="dropdown-item" href="#">Edit book</button>
                    <button type="button" class="dropdown-item" href="#">Delete book</button>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav col-2">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn-block" href="#" id="authorMenu" role="button" id="dropDownAuthor"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Author
                </a>
                <div id="dropdownAuthMenu"  class="dropdown-menu" aria-labelledby="navbarAuthor">
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#addAuthorModal">Add author</button>
                    <button type="button" class="dropdown-item">Edit current author</button>
                </div>
            </li>
        </ul>

        <jsp:include page="headers.jsp"/>
        <script>
        /*   $('#dropdownLink').on('click',
                function () {
                    $('a #dropdownBookMenu').show();
                    $('li .nav-item .dropdown').show();
                });*/
        </script>
        <script src="webjars/jquery/3.3.1/jquery.min.js"></script>
        <script src="webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
        <script src="webjars/popper.js/1.14.1/umd/popper.min.js"></script>
        <script src="webjars/popper.js/1.14.1/umd/popper-utils.js"></script>
        <script src="webjars/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    </nav>

    <jsp:include page="addBookModal.jsp"/>
    <jsp:include page="addAuthorModal.jsp"/>
