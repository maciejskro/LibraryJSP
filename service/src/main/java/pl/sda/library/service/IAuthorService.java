package pl.sda.library.service;

import pl.sda.library.dto.AuthorDTO;
import pl.sda.library.entity.Author;

import java.util.List;

public interface IAuthorService {

    List<AuthorDTO> findAllAuthors();

    Boolean addAuthor(AuthorDTO authorDTO);
}
