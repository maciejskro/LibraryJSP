<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: smaciej
  Date: 06.06.18
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Book list</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.1/css/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.1/css/bootstrap-grid.min-jsf.css"/>
    <script src="webjars/jquery/3.3.1/jquery.min.js"></script>
    <script src="webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
    <script src="webjars/popper.js/1.14.1/umd/popper.min.js"></script>
    <script src="webjars/popper.js/1.14.1/umd/popper-utils.js"></script>
    <script src="webjars/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<!--boook -->
<div class="container justify-content-center mt-3">
    <div class="row align-content-center ">
        <div class="col-12">
            <!-- header hidden -->
            <jsp:include page="WEB-INF/headers_navi.jsp"/>
        </div>
    </div>
    <div class="row align-content-stretch">
        <div class="col-12">
            <!-- booktable -->
            <jsp:include page="booktable.jsp"/>
        </div>
    </div>

    <div class="row align-content-end">
        <div class="col-12">
            <%@ include file="WEB-INF/footer.jsp" %>
        </div>
    </div>
</div>

</body>
</html>