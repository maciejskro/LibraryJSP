<%--
  Created by IntelliJ IDEA.
  User: mskrobiszewski
  Date: 05.06.18
  Time: 18:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.1/css/bootstrap-grid.min-jsf.css"/>
<link rel="stylesheet" href="${pageContext.request.contentPath}/webjars/datatables/1.10.16/css/jquery.dataTables.min.css"/>
<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
<script src="webjars/popper.js/1.14.1/umd/popper.min.js"></script>
<script src="webjars/popper.js/1.14.1/umd/popper-utils.js"></script>
<script src="webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
<script src="webjars/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

        <form action="/homeservlet" type="post">
            <table class="table table-hover">
                <thead>
                   <tr>
                       <th>Lp.</th>
                       <th>Title</th>
                       <th>Authors</th>
                       <th>ISBN</th>
                       <th>Category</th>
                       <th>Release</th>
                       <th>Pages</th>
                       <th>Select book</th>
                   </tr>
                </thead>
                <tbody>
                 <c:forEach items="${sessionScope.books}" var="book" varStatus="loop">
                     <tr>
                         <td>${loop.index+1}  </td>
                         <td>${book.title}</td>
                         <td>${book.authorName}</td>
                         <td>${book.ISBN}</td>
                         <td>${book.booksType}</td>
                         <td>${book.releaseDate}</td>
                         <td>${book.pages}</td>
                         <td><input type="radio" class="from-check-input radio-menu-item" name="selectBook" value="${book.id}"></td>
                     </tr>
                 </c:forEach>
                </tbody>
            </table>
        </form>