package pl.sda.library.controller;

import pl.sda.library.dto.AuthorDTO;
import pl.sda.library.dto.BookDTO;
import pl.sda.library.dto.BorrowerDTO;
import pl.sda.library.entity.BooksType;
import pl.sda.library.service.AuthorService;
import pl.sda.library.service.BookService;
import pl.sda.library.service.IAuthorService;
import pl.sda.library.service.IBookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/homeservlet")
public class HomeServlet extends HttpServlet {

    private final IBookService bookService;
    private final IAuthorService authorService;

    public HomeServlet() {
        this.bookService = new BookService();
        this.authorService = new AuthorService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);

        List<BookDTO> books = bookService.findAll();
        List<AuthorDTO> authors = authorService.findAllAuthors();
        session.setAttribute("books" , books);
        session.setAttribute("category" ,BooksType.values() );
        session.setAttribute("authorsList" ,authors);
        //req.setAttribute("logeduser" , session.getAttribute("logeduser"));
        req.getRequestDispatcher("book.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       // Long bookId =  Long.valueOf( req.getParameter("bookId"));
       // req.getParameter("action");

        HttpSession session = req.getSession(false);
        String action = req.getParameter("action");
        BorrowerDTO user = (BorrowerDTO) session.getAttribute("logeduser");
        resp.sendRedirect("/homeservlet");

    }
}
