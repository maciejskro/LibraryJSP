<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="resources/floating-labels.css"/>
<div class="container">
    <!-- Modal Author-->
    <div class="modal fade" id="addAuthorModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add author</h4>
                </div>
                <form action="authorservlet" method="post" class="form-signin-modal" id="addauthor_form"
                      name="dialogAddBook">
                    <div class="modal-body">

                        <div class="form-label-group">
                            <input type="text" class="form-control" aria-labelledby="firstnameInput" name="firstName"
                                   id="firstnameInput" placeholder="Enter first name " required autofocus>
                            <label for="firstnameInput">Enter first name</label>
                        </div>

                        <div class="form-label-group">
                            <input type="text" class="form-control" aria-labelledby="lastnameInput" name="lastName"
                                   id="lastNameInput" placeholder="Enter last name" required autofocus>
                            <label for="lastNameInput">Insert last name or author surname </label>
                        </div>

                        <div class="form-label-group">
                            <input type="text" class="form-control" aria-labelledby="bornPlaceInput" name="placeOfBorn"
                                   id="placeOfBornInput" placeholder="Enter place of born's author" required autofocus>
                            <label for="placeOfBornInput">Place of born</label>
                        </div>

                        <div class="form-label-group">
                            <input type="date" class="form-control date-picker" aria-labelledby="dateBornInput"
                                   name="dateBorn"
                                   id="dateBornInput" placeholder="Enter born date" required autofocus>
                            <label for="dateBornInput">Born date</label>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="submitAuthorForm" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    let authors = $('#authorListSelect').val();
    $(function () {
            $("#submitAuthorForm").on("submit", function (e) {
                e.preventDefault();
                let postData = $('#addauthor_form').serialize();
                let formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data) {
                        $('#addAuthorModal .modal-header .modal-title').html("Result");
                        $('#addAuthorModal .modal-body').html(data).update("Saved successfully !");
                        $('#submitAuthorForm').remove();
                    },
                    error: function (jqXHR, status, error) {
                        console.log(status + ": " + error + " " + authors);
                    }
                });
            });
        }
    );
</script>