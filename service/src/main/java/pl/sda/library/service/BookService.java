package pl.sda.library.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import pl.sda.library.dto.BookDTO;
import pl.sda.library.entity.Author;
import pl.sda.library.entity.Book;
import pl.sda.library.entity.BooksType;
import pl.sda.library.exception.AuthorNotFoundException;
import pl.sda.library.exception.ItemNoFoundException;
import pl.sda.library.model.AuthorRepository;
import pl.sda.library.model.BookRepository;
import pl.sda.library.model.IBaseRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookService implements IBookService  {

    private IBaseRepository<Book> bookRepository;
    private IBaseRepository<Author> authotRepository;

    public BookService() {
        this.bookRepository = new BookRepository();
        this.authotRepository = new AuthorRepository();
    }

    @Override
    public void removeBook(Book i) {

    }

    @Override
    public BookDTO find(Long id) throws ItemNoFoundException {
        return null;
    }

    @Override
    public List<BookDTO> findAll() {
        List<Book> bookList = bookRepository.findAll();
        return bookList.stream()
                .map( b ->  { BookDTO result = new BookDTO();
                            result.setTitle(b.getTitle());
                            result.setReleaseDate(b.getReleaseDate());
                            result.setAuthorName(result.getAuthors(b.getAuthors()));
                            result.setBooksType( b.getBooksType().name() );
                            result.setISBN(b.getISBN());
                            result.setPages(b.getPages());
                        return  result;})
                .collect(Collectors.toList());
    }

    @Override
    public List<String> findAllCategory() {
        List<String> result = Arrays.stream(BooksType.values())
                .map(BooksType::getDescription)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public Boolean addBook(BookDTO bookdto) throws  AuthorNotFoundException {
        List<Author> authorList = new ArrayList<>();
        for (String s : bookdto.getAutorIdList()) {
            Author a = Optional.ofNullable(authotRepository.find( Long.valueOf(s))).orElseThrow(
                    () -> {
                        return new AuthorNotFoundException("author not found");
                    }
            );
            authorList.add(a);
        }
        Book book = createBook(bookdto);
        book.setAuthors(authorList);
         if (bookRepository.create(book)!=null) {
             return true;
         } else return false;
    }



    @Override
    public Boolean editBook(BookDTO book) {
        return null;
    }

    private Book createBook(BookDTO bookdto) {
        Book result = new Book();
            result.setBooksType(BooksType.valueOf(bookdto.getBooksType()));
            result.setDescription(bookdto.getDescription());
            result.setISBN(bookdto.getISBN());
            result.setPages(bookdto.getPages());
            result.setReleaseDate(bookdto.getReleaseDate());
            result.setTitle(bookdto.getTitle());
        return result;
    }

    private MapperFactory getMapperFactory() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        return mapperFactory;
    }
}
