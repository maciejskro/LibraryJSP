package pl.sda.library.controller;

import pl.sda.library.dto.AuthorDTO;
import pl.sda.library.dto.BookDTO;
import pl.sda.library.entity.Author;
import pl.sda.library.service.AuthorService;
import pl.sda.library.service.BookService;
import pl.sda.library.service.IAuthorService;
import pl.sda.library.service.IBookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet("/addbookservlet")
public class AddBookServlet extends HttpServlet {

    private final IAuthorService authorservice;
    private final IBookService bookservice;

    public AddBookServlet() {
        this.authorservice = new AuthorService();
        this.bookservice = new BookService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        session.setMaxInactiveInterval(10);
        BookDTO bookdto = createBookDto(req);
        session.setAttribute("book", bookdto);
        boolean result;
        try {
            result = bookservice.addBook(bookdto);
            System.out.println(bookdto.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
            resp.sendRedirect("/homeservlet");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("jestem w get");
    }

    private BookDTO createBookDto(HttpServletRequest req) {
        BookDTO result = new BookDTO();
            result.setTitle(req.getParameter("title"));
            result.setISBN(req.getParameter("isbn"));
            result.setBooksType(req.getParameter("category"));
            result.setAutorIdList(
                    (String[]) Arrays.asList(req.getParameterValues("authors")).toArray() );
            result.setPages(Integer.parseInt(req.getParameter("pages")));
            result.setReleaseDate(LocalDate.parse(req.getParameter("release")));
            result.setDescription(req.getParameter("description"));

        System.out.println(req.getParameter("title") + " " + req.getParameterValues("authors").length + " " + req.getParameter("category"));
        return result;
    }

}
