package pl.sda.library.service;

import pl.sda.library.dto.AuthorDTO;
import pl.sda.library.entity.Author;
import pl.sda.library.model.AuthorRepository;
import pl.sda.library.model.IBaseRepository;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorService implements IAuthorService {

    private IBaseRepository<Author> authorRepository;

    public AuthorService() {
        this.authorRepository = new AuthorRepository();
    }

    @Override
    public List<AuthorDTO> findAllAuthors() {
        List<Author> authors = this.authorRepository.findAll();
        return authors.stream()
                .map(author -> {AuthorDTO a = new AuthorDTO();
                        a.setId(author.getId());
                        a.setFirstname(author.getFirstname());
                        a.setLastname(author.getLastname());
                        a.setPlaceOfBorn(author.getPlaceOfBorn());
                        return a;})
                .collect(Collectors.toList());
    }

    @Override
    public Boolean addAuthor(AuthorDTO authorDTO) {
        return null;
    }

}
