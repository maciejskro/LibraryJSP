package pl.sda.library.controller;

import pl.sda.library.dto.AuthorDTO;
import pl.sda.library.service.AuthorService;
import pl.sda.library.service.IAuthorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/authorservlet")
public class AuthorServlet extends HttpServlet {

    private IAuthorService authorService;

    public AuthorServlet() {
        this.authorService = new AuthorService();
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        session.setMaxInactiveInterval(10);
        AuthorDTO authorDTO = createAuthor(req);
        boolean result;
        try {
            result = authorService.addAuthor(authorDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/homeservlet");
    }

    private AuthorDTO createAuthor(HttpServletRequest req) {
        AuthorDTO auth = new AuthorDTO();
            auth.setFirstname(req.getParameter(""));
            auth.setLastname(req.getParameter(""));
            auth.setPlaceOfBorn(req.getParameter(""));
            auth.setDateOfBorn(LocalDate.parse(req.getParameter("")));
        return auth;
    }
}
